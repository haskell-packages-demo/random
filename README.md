# random

S[R](https://repology.org/project/haskell:random/versions)ArDeFeNiSu Pseudo-random number generation [random](https://hackage.haskell.org/package/random)

# Books, articles, or wikis
* [*Haskell/Libraries/Random*
  ](https://en.wikibooks.org/wiki/Haskell/Libraries/Random)
  2020-04 WikiBooks
* *Real World Haskell
  * Chapter 13. Data Structures
  * up-to-date-real-world-haskell
    * https://github.com/tssm/up-to-date-real-world-haskell/blob/master/13-data-structures.org
* *School of Haskell*
  2014-03
  * [*Random numbers in Haskell*
    ](https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/randoms)
* *Learn you a Haskell for great good! : a beginner's guide*
  2012
  * 9. More Input and More Output

# Examples
* [haskell random example](https://google.com/search?q=haskell+random+example)
