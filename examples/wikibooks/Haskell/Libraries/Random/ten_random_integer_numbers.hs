import Prelude
    ( ($)
    , Int
    , IO
    , print
    , take
    )
import System.Random
    ( newStdGen
    , randoms
    )
-----
main :: IO ()
main = do
   gen <- newStdGen
   let ns = randoms gen :: [Int]
   print $ take 10 ns
